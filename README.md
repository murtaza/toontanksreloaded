# Toon Tanks Reloaded - Unreal Engine

Created and designed a labyrinth type level where you play as a Tank with the goal of navigating through the map and destroying every turret in order to win.

The game has SFX as well as enemy AI with aiming and shooting mechanics.

Game restarts after 5 seconds of winning or losing. (Win by eliminating all turrets)

### Toon Tanks Gameplay

![](images/ToonTanks.gif)

## How to install?
1. Download the Build/Windows directory 
2. Extract contents into a folder
3. Open up the Build/Windows/ToonTanks.exe

## How to play?
Move forward, backwards, left, and right with W (forward),  S (backwards), A (left) and D (right) keys.

Aim with the mouse and shoot with LMB
